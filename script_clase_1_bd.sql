/*
comentario de varias lineas
*/

#comentario de una sola linea

#Consultar las bases de datos
SHOW DATABASES;

#Crear una BD
CREATE DATABASE universidad_udea;

#Conectar a BD a traves de la pestaña esquema

/*
---------------------------CREAR TABLAS-------------------
*/

CREATE TABLE universidad(
    id INTEGER PRIMARY KEY AUTO_INCREMENT NOT NULL,
    nombre VARCHAR(100) NOT NULL,
    email VARCHAR (100) NOT NULL,
    direccion VARCHAR (100) NOT NULL
);

#consultar tablas
SHOW TABLES;

#Insertar datos/registros en la tabla

INSERT INTO universidad (nombre,email,direccion) VALUES ("UDEA","academica@udea.edu.co","CR 100 CL 44 -02");

#Consultar todos los datos desde universidad de una tabla

SELECT * FROM universidad;

INSERT INTO universidad (nombre,email,direccion) VALUES ("Universidad de Antioquia","info@udea.edu.co","CR 100 CL 44-02");
INSERT INTO universidad (nombre,email,direccion) VALUES ("UTC","info@utc.edu.co","CR 23 CL 100 -44");
INSERT INTO universidad (nombre,email,direccion) VALUES ("UTD","info@utd.edu.co","CR 200 CL 100 -44");
INSERT INTO universidad (nombre,email,direccion) VALUES ("UTO","info@uto.edu.co","CR 10 CL 10 -44");
INSERT INTO universidad (nombre,email,direccion) VALUES ("UTF","info@utf.edu.co","CR 222 CL 110 -44");
INSERT INTO universidad (nombre,email,direccion) VALUES ("UTS","info@uts.edu.co","CR 2 CL 100 -44");
INSERT INTO universidad (nombre,email,direccion) VALUES ("UTT","info@utt.edu.co","CR 44 CL 100 -44");


SELECT * FROM universidad;

#se debe de colocar where para que no se eliminen todos los registros como en el siguiente codigo
DELETE FROM universidad;

DELETE FROM universidad WHERE id=2;
DELETE FROM universidad WHERE id=4;

#Eliminar Tabla

DROP TABLE universidad;

#---------------WHERE Y SELECCION DE DATOS-----------------
#traer los datos de una columna en especifico
SELECT nombre from universidad;
#obtener datos de mas columnas
SELECT nombre, email FROM universidad;
#Uso de alias para una columna

SELECT nombre AS universidad, email AS correo FROM universidad;
#tambien se puede un alias sin el AS solo con espaciado

SELECT nombre universidad, email correo FROM universidad;
#-----------USO DE FUNCIONES-----------------

#Obtener la cantidad de registros de la tabla
SELECT COUNT(*) cant_universidades FROM universidad;

#Obtener la sumatoria de todos los ids
SELECT SUM(id) sumatoria_id FROM universidad;

#------------WHERE----------
SELECT id,nombre FROM universidad WHERE id>=8;
SELECT id,nombre FROM universidad WHERE id=1 OR id=8;
SELECT id,nombre FROM universidad WHERE id!=1;
SELECT id,nombre FROM universidad WHERE nombre="UDEA";

#-------------WHERE Y LIKE--------------

#Trae los datos que comiencen con UD
SELECT * FROM universidad WHERE nombre lIKE "UD%";
#Trae los datos que terminen en C
SELECT * FROM universidad WHERE nombre LIKE "%C";
#Datos que tengan la letra en comun A 
SELECT * FROM universidad WHERE nombre LIKE "%A%";

